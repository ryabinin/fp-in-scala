package fpinscala.ch04

import scala.annotation.tailrec

object OptionLift {
  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f

  import java.util.regex._

  def pattern(s: String): Option[Pattern] =
    try {
      Some(Pattern.compile(s))
    } catch {
      case e: PatternSyntaxException => None
    }

  def mkMatcher(pat: String): Option[String => Boolean] =
    pattern(pat) map (p => (s: String) => p.matcher(s).matches)

  def bothMatch(pat: String, pat2: String, s: String): Option[Boolean] =
    for {
      f <- mkMatcher(pat)
      g <- mkMatcher(pat2)
    } yield f(s) && g(s)

  // EX 4
  def bothMatch_2(pat1: String, pat2: String, s: String): Option[Boolean] =
    Option.map2(mkMatcher(pat1), mkMatcher(pat2))((m1, m2) => m1(s) && m2(s))

  // EX 5
  def sequence[A](a: List[Option[A]]): Option[List[A]] = {
    @tailrec
    def sequenceRec(prev: Option[List[A]], aList: List[Option[A]]): Option[List[A]] = aList match {
      case Nil => prev
      case None :: _ => None
      case x :: xs =>
        val headWithPrev: Option[List[A]] = Option.map2(prev, x.flatMap(el => Some(List(el)))) {
          case (l1, l2) => l1 ++ l2
        }
        sequenceRec(headWithPrev, xs)
    }

    sequenceRec(Some(List.empty), a)
  }

  // EX 6
  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = {
    @tailrec
    def traverseRec(prev: Option[List[B]], aList: List[A]): Option[List[B]] = aList match {
      case Nil => prev
      case x :: xs =>
        f(x) match {
          case None => None // short circuit
          case s@Some(v) =>
            val headWithPrev: Option[List[B]] = Option.map2(prev, s.flatMap(el => Some(List(el)))) {
              case (l1, l2) => l1 ++ l2
            }
            traverseRec(headWithPrev, xs)
        }
    }

    traverseRec(Some(List.empty), a)
  }

  def sequenceByTraverse[A](a: List[Option[A]]): Option[List[A]] = traverse(a)(a => a)
}


