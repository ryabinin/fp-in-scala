package fpinscala.ch04

trait Option[+A] {
  def map[B](f: A => B): Option[B] = this match {
    case Some(v) => Some(f(v))
    case None => None
  }
  def flatMap[B](f: A => Option[B]): Option[B] = this match {
    case Some(v) => f(v)
    case None => None
  }
  def getOrElse[B >: A](default: => B): B = this match {
    case Some(v) => v
    case None => default
  }
  def orElse[B >: A](ob: => Option[B]): Option[B] = this match {
    case Some(v) => this
    case None => ob
  }
  def filter(f: A => Boolean): Option[A] = this.flatMap(v => if (f(v)) Some(v) else None)
}

object Option {
  // EX 3
  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = for {
    aa <- a
    bb <- b
  } yield f(aa, bb)
}

case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]

object Variance {
  def variance(xs: Seq[Double]): Option[Double] = ???
}