package fpinscala.ch04

import scala.util.Try

object Main extends App {
  println(OptionLift.sequence(List(Some("a"), Some("b"), Some("c"))))
  println(OptionLift.sequence(List(Some("a"), Some("b"), None)))
  println(OptionLift.sequence(List(Some("a"), None)))
  println(OptionLift.sequence(List(None)))
  println(OptionLift.sequence(List.empty))

  println()

  def sToOptI: String => Option[Int] = s => try {
    Some(s.toInt)
  } catch {
    case _: NumberFormatException => None
  }

  println(OptionLift.traverse(List("1", "2", "3"))(sToOptI))
  println(OptionLift.traverse(List("1", "2", ""))(sToOptI))
  println(OptionLift.traverse(List("a", ""))(sToOptI))
  println(OptionLift.traverse(List(""))(sToOptI))
  println(OptionLift.traverse(List.empty[String])(sToOptI))

  println()

  println(Either.sequence(List(Right("1"), Right("2"), Right("3"))))
  println(Either.sequence(List(Right("1"), Right("2"), Left("ERROR"))))
  println(Either.sequence(List(Right("1"), Left("ERROR"))))
  println(Either.sequence(List(Left("ERROR 1"), Left("ERROR 2"))))
  println(Either.sequence(List.empty))

  println()

  val person = for {
    age <- Right(42)
    name <- Left("invalid name")
    salary <- Right(1000000.0)
  } yield (name, age, salary)

  println(person)

  println()
  case class Person(name: Name, age: Age)

  sealed class Name(val value: String)

  sealed class Age(val value: Int)

  def mkName(name: String): Either[String, Name] =
    if (name == "" || name == null) Left("Name is empty.")
    else Right(new Name(name))

  def mkAge(age: Int): Either[String, Age] =
    if (age < 0) Left("Age is out of range.")
    else Right(new Age(age))

  def mkPerson(name: String, age: Int): Either[String, Person] =
    mkName(name).map2(mkAge(age))(Person(_, _))

  println(mkPerson("", 1))
  println(mkPerson("", -1))
  println(mkPerson("A", -1))
  println(mkPerson("A", 1))
}