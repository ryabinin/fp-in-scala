package fpinscala.ch04

// EX 7
sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B] = this match {
    case Right(v) => Right(f(v))
    case t@Left(_) => t
  }
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] =  this match {
    case Right(v) => f(v)
    case t@Left(_) => t
  }

  def orElse[EE >: E,B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
    case Right(v) => this
    case Left(_) => b
  }

  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = for {
    v <- this
    vb <- b
  } yield f(v, vb)
}

object Either {
  // EX 8
  def sequence[E, A](a: List[Either[E, A]]): Either[E, List[A]] = {
    def sequenceRec(prev: Either[E, List[A]], aList: List[Either[E, A]]): Either[E, List[A]] = aList match {
      case Nil => prev
      case Left(e) :: _ => Left(e)
      case x :: xs =>
        val headEither = x.flatMap(v => Right(List(v)))
        val headWithPrev: Either[E, List[A]] = prev.map2(headEither) {
          case (a, b) => a ++ b
        }
        sequenceRec(headWithPrev, xs)
    }

    sequenceRec(Right(List.empty[A]), a)
  }

  def traverse[E, A, B](a: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    def sequenceRec(prev: Either[E, List[B]], aList: List[A]): Either[E, List[B]] = aList match {
      case Nil => prev
      case x :: xs =>
        f(x) match {
          case l@Left(e) => l // short circuit
          case r@Right(v) =>
            val headEither = r.flatMap(v => Right(List(v)))
            val headWithPrev: Either[E, List[B]] = prev.map2(headEither) {
              case (a, b) => a ++ b
            }
            sequenceRec(headWithPrev, xs)
        }
    }

    sequenceRec(Right(List.empty[B]), a)
  }
}
case class Left[+E](value: E) extends Either[E, Nothing]
case class Right[+A](value: A) extends Either[Nothing, A]