package fpinscala.ch02

import scala.annotation.tailrec

object Exercises extends App {
  // EX 1
  def fib(n: Int): Int = {
    @tailrec
    def rec(prev1: Int, prev2: Int, n: Int): Int = {
      if (n == 0) {
        prev1
      } else {
        rec(prev2, prev1 + prev2, n - 1)
      }
    }

    rec(0, 1, n)
  }

  // EX 2
  def isSorted[A](as: Array[A], gt: (A,A) => Boolean): Boolean = {
    @tailrec
    def func(startIndex: Int): Boolean = {
      if (startIndex <= (as.length - 2)) {
        gt(as(startIndex), as(startIndex + 1)) && func(startIndex + 1)
      } else {
        true
      }
    }
    func(0)
  }

  // EX 3
  def curry[A,B,C](f: (A, B) => C): A => (B => C) = a => ( b => f(a, b) )

  // EX 4
  def uncurry[A,B,C](f: A => B => C): (A, B) => C = (a, b) => f(a)(b)

  // EX 5
  def compose[A,B,C](f: B => C, g: A => B): A => C = a => f(g(a))


  val m: List[(Int, Int)] = List((0, 0), (1, 1), (2, 1), (3, 2), (4, 3), (5, 5), (6, 8), (7, 13), (8, 21), (9, 34),
    (10, 55), (11, 89), (12, 144), (13, 233), (14, 377), (15, 610), (16, 987), (17, 1597), (18, 2584), (19, 4181),
    (20, 6765), (21, 10946), (22, 17711), (23, 28657), (24, 46368), (25, 75025), (26, 121393), (27, 196418), (28, 317811), (29, 514229))

  m.foreach {
    case (n, expected) =>
      val calculated = fib(n)
      if (calculated != expected) {
        println(s"Wrong fib number for n=$n. Expected=$expected but actual is $calculated")
      } else {
        println(s"Fine for $n")
      }
  }

  val fff: (Int, Int) => Boolean = (i1, i2) => i1 <= i2
  println(isSorted(Array(1, 2, 3), fff))
  println(isSorted(Array(1, 2, 1), fff))
  println(isSorted(Array(11, 2, 3), fff))
  println(isSorted(Array(11, 12), fff))
  println(isSorted(Array(11, 0), fff))
  println(isSorted(Array(), fff))

  val ff_curried: Int => (String => String) = curry((ii1: Int, ii2: String) =>  "" + ii1 + ii2)
  val funcThatAcceptsString: String => String = ff_curried(111)
  println(funcThatAcceptsString("boom"))

}
