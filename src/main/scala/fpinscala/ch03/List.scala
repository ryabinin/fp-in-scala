package fpinscala.ch03

import scala.annotation.tailrec

sealed trait List[+A]

case object Nil extends List[Nothing]

case class Cons[+A](head: A, tail: List[A]) extends List[A] {
}

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  // EX 2
  def tail[A](list: List[A]): List[A] = list match {
    case Cons(_, tail) => tail
    case Nil => throw new RuntimeException("Tail of an empty list")
  }

  // EX 3
  def setHead[A](list: List[A], head: A): List[A] = list match {
    case Cons(_, tail) => Cons(head, tail)
    case Nil => Cons(head, Nil)
  }

  // EX 4
  @tailrec
  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Nil => Nil
    case Cons(_, _) if n == 0 => l
    case Cons(_, tail) if n != 0 => drop(tail, n - 1)
  }

  // EX 5
  @tailrec
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case Cons(head, _) if !f(head) => l
    case Cons(head, tail) if f(head) => dropWhile(tail, f)
  }

  def append[A](a1: List[A], a2: List[A]): List[A] =
    a1 match {
      case Nil => a2
      case Cons(h,t) => Cons(h, append(t, a2))
    }

  // EX 6
  def init[A](l: List[A]): List[A] = {
    @tailrec
    def rec(newL: List[A], l: List[A]): List[A] = l match {
      case Nil => newL
      case Cons(_, Nil) => newL
      case Cons(x, xs) => rec(append(newL, Cons(x, Nil)), xs)
    }

    rec(Nil, l)
  }

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def sum2(ns: List[Int]): Int = foldRight(ns, 0)((x, y) => x + y)

  def product2(ns: List[Double]): Double = foldRight(ns, 1.0)(_ * _)
}