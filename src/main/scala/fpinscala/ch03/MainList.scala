package fpinscala.ch03

object MainList extends App {
  println(List.init(Nil))
  println(List.init(Cons(1, Nil)))
  println(List.init(Cons(1, Cons(2, Nil))))
  println(List.init(Cons(1, Cons(2, Cons(3, Nil)))))
}